<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Collaboration with Others

In [episode 9](09-remotes-in-gitlab.md), you already learnt about contributing changes via the local Git repository and the GitLab Web interface.
In this episode, we show you how you can collaborate with other persons on the basis of [Git branches](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell) and [GitLab Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html).
[branching.pptx](episodes/extras/branching.pptx) provides a summary of the performed steps.

## Preparation: Add new Collaborators to your Gitlab Project

First, you have to make sure that other persons can contribute to the remote Git repository.
For that purpose, you can add [new collaborators to the `planets` GitLab project](https://gitlab.com/help/user/project/members/index.md) with `Developer` permission or higher (see also: [GitLab's documentation about project member permissions](https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions)).

> **Please Note:** Basically, it is sufficient that a contributor has read access to the Git repository of your GitLab project to contribute to it (in private projects: permission `Reporter` or higher, in internal/public projects: all logged in GitLab users).
> In this case, he/she can contribute via the [fork workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html).
> This workflow includes some additional steps but it basically works quite similar to the approach shown below.

## Create a new Contribution (Collaborator View)

In the following, we work from the collaborator's point of view and want to contribute basic project documentation.

- Clone the remote Git repository:
   - `git clone <REMOTE URL>`
   - Show where to retrieve the URL in Gitlab
- Show current state of the cloned local Git repository:
   - `git graph`
   - Highlight current branches
- Create and switch to a local branch:
   - `git branch documentation`
   - `git checkout documentation` (Git version >= 2.23 supports: `git switch documentation`)
- Show current state of the repository:
   - `git graph`
   - Highlight new branches pointing to the same commit
- Create, change and commit a `README.md`:
   - `touch README.md`
   - Add the line `This repository contains notes about potential planets for migration.` to README.md
   - `git add README.md`
   - `git commit -m "Add description for repository"`
- Show current state of the repository:
   - `git graph`
   - Highlight the new commit on `documentation`
- Push new branch to the remote repository:
  - Try `git push`, note `--set-upstream` hint & apply it 
- Show how a [merge request is created](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html):
   - Add a **good** title: `Add repository documentation`
   - Explain what can be put into the description (e.g., checklists for developer and reviewer)
   - Mention that merge requests can be created from issue tracker
   - Remove `WIP` (work in progress status) as last step to show collaboration is done.

## Review the Contribution (Owner View)

Now, we switch into the role of the project owner and review the contribution.

- Switch to the newly created merge request and review it (see also: [GitLab's documentation about managing merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/reviewing_and_managing_merge_requests.html)):
  - Explain about basic aspects to check:
    - Is the scope of the contribution clear?
    - Is it working?
    - Is it sound and concise?
  - Show how to interact via comments
  - Show `Insert suggestion` functionality
- Merge changes into `master`:
  - Click the `Merge` button

## Pull Changes (Collaborator View)

Then, the collaborator pulls the resulting `master` branch to make sure that the local Git repository is up-to-date.
For that purpose, we switch to our local Git repository.

- Switch to branch `master`:
   - `git checkout master` (Git version >= 2.23 supports: `git switch master`)
- Fetch latest changes without merging:
   - `git fetch -v`
   - Explain output
- Show current state of the repository:
   - `git graph`
   - Highlight that both `master` branches now point to the merge commit

## Mark the Results using a Tag (Owner View)

Finally, the owner marks the current state via a tag.

- [Tags are human-usable pointer to mark important points in the version history](https://www.endoflineblog.com/img/oneflow/hotfix-branch-merge-final.png)
  (similar to branch names & `HEAD`)
- Create a tag:
  - `git tag v0.1 -m "Mark initial version"`
- Push the tag to the remote repository:
  - `git push origin v0.1`
  - Explain that `--tags` will push all tags at once
  - Explain that `v0.1`, `HEAD`, a commit ID and a branch name can refer to the same commit
- Show the tag in GitLab

## Key Points

- Branches allow you to track code changes for different tasks in parallel.
- Merge requests should be used for reviews and visual tracking of changes before merge.
- `git fetch` can be used to update information of tracked remotes.
- Tags help you to mark important versions explicitly.
